package ictgradschool.industry.lab18.ex02;

/**
 * Created by glee156 on 16/01/2018.
 */
public interface Observer {
    void addObserver(Observer o);
    void removeObserver(Observer o);
    void update();
}

