package ictgradschool.industry.lab18.ex02;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by glee156 on 16/01/2018.
 */
public class SnakeCanvas extends JPanel implements Observer {

    private ArrayList<Integer> redXCoordinateList;
    private ArrayList<Integer> snakeXCoordinateList;
    private ArrayList<Integer> redYCoordinateList;
    private ArrayList<Integer> snakeYCoordinateList;
    private int greenXCooordinate;
    private int greenYCoordinate;
    private boolean isGameOver;
    Program program;
    BufferedImage img = null;
    BufferedImage andrew = null;

    SnakeCanvas(Program program){
        this.program = program;
        program.addObserver(this);

        try {
            img = ImageIO.read(new File("biggest.png"));
        } catch (IOException e) {
        }

        try {
            andrew = ImageIO.read(new File("andrew.jpg"));
        } catch (IOException e) {
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < snakeXCoordinateList.size(); i++) {
//            g.setColor(Color.gray);
//            g.fill3DRect(snakeXCoordinateList.get(i) * 25 + 1, snakeYCoordinateList.get(i) * 25 + 1, 23, 23, true);
            g.drawImage(img, snakeXCoordinateList.get(i) * 25 + 1, snakeYCoordinateList.get(i) * 25 + 1, 23, 23,null);
        }
        g.setColor(Color.green);
        g.fill3DRect(greenXCooordinate * 25 + 1, greenYCoordinate * 25 + 1, 23, 23, true);
        for (int i = 0; i < redXCoordinateList.size(); i++) {
//            g.setColor(Color.red);
//            g.fill3DRect(redXCoordinateList.get(i) * 25 + 1, redYCoordinateList.get(i) * 25 + 1, 23, 23, true);
            g.drawImage(andrew, redXCoordinateList.get(i) * 25 + 1, redYCoordinateList.get(i) * 25 + 1, 23, 23,null);
        }
        if (isGameOver) {
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 60));
            FontMetrics fm = g.getFontMetrics();
            g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
        }
    }

    @Override
    public void addObserver(Observer o) {

    }

    @Override
    public void removeObserver(Observer o) {

    }

    @Override
    public void update() {
        //update values for all coordinates
        greenXCooordinate = program.getGreenXCooordinate();
        greenYCoordinate = program.getGreenYCoordinate();
        redXCoordinateList = program.getRedXCoordinateList();
        redYCoordinateList = program.getRedYCoordinateList();
        snakeXCoordinateList = program.getSnakeXCoordinateList();
        snakeYCoordinateList = program.getSnakeYCoordinateList();
        isGameOver = program.isGameOver();
    }
}
