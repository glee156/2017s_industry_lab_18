package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.List;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random randomGenerator = new Random();
    private int greenXCooordinate, greenYCoordinate;
    private ArrayList<Integer> redXCoordinateList = new ArrayList<>();
    private ArrayList<Integer> snakeXCoordinateList = new ArrayList<>();
    private ArrayList<Integer> redYCoordinateList = new ArrayList<>();
    private ArrayList<Integer> snakeYCoordinateList = new ArrayList<>();
    private int xCoordinate;
    private int yCoordinate;
    private int directionCode =39;
    private boolean isGameOver = false;
    List<Observer> observerList = new ArrayList<>();
    private SnakeCanvas canvas = new SnakeCanvas(this);


    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {
        //Generate snake coordinates
        for (int i = 0; i < 6; i++) {
            snakeXCoordinateList.add(10 - i);
            snakeYCoordinateList.add(10);
        }
        //Generate square coordinates
        createGreenSquare();
        createRedSquare();

        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        canvas.setBackground(Color.white);
        add(BorderLayout.CENTER, canvas);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int inputKeyCode= e.getKeyCode();
                //Check for invalid key input
                if ((inputKeyCode >= 37) && (inputKeyCode <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.directionCode - inputKeyCode) != 2) {// block moving back
                        Program.this.directionCode = inputKeyCode;
                    }
                }
            }
        });
        setVisible(true);
    }

    void go() { // main loop
        while (!isGameOver) {
            move();

            isGameOver = checkHasHitRedSquare(xCoordinate, yCoordinate) || checkHasHitSelf(xCoordinate, yCoordinate);
            snakeXCoordinateList.add(0, xCoordinate);
            snakeYCoordinateList.add(0, yCoordinate);

            //check if snake coordinates match green square coordinates ie. snake has 'eaten' green square. If it has eaten
            //the green square, generate new coordinates for green and red square. Otherwise, move snake by removing last element
            //in snake coordinates list
            if (((snakeXCoordinateList.get(0) == greenXCooordinate) && (snakeYCoordinateList.get(0) == greenYCoordinate))) {
                createGreenSquare();
                createRedSquare();
                setTitle("Program" + " : " + snakeXCoordinateList.size());
            } else {
                //removing last square of snake for move animation
                snakeXCoordinateList.remove(snakeXCoordinateList.size() - 1);
                snakeYCoordinateList.remove(snakeYCoordinateList.size() - 1);
            }

            //paint with new coordinates for new green and red square
            update();
            canvas.repaint();

            //determines how often repaint() gets called by making main thread sleep
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void move() {
        xCoordinate = snakeXCoordinateList.get(0);
        yCoordinate = snakeYCoordinateList.get(0);
        if (directionCode == 37) {
            //Moves Left
            xCoordinate--;
        }
        if (directionCode == 39) {
            xCoordinate++;
            //Moves Right
        }
        if (directionCode == 38) {
            yCoordinate--;
            //Moves up
        }
        if (directionCode == 40) {
            yCoordinate++;
            //Moves down
        }
        //Wraps snake around if hits wall
        if (xCoordinate > 30 - 1) {
            xCoordinate = 0;
        }
        if (xCoordinate < 0) {
            xCoordinate = 30 - 1;
        }
        if (yCoordinate > 20 - 1) {
            yCoordinate = 0;
        }
        if (yCoordinate < 0) {
            yCoordinate = 20 - 1;
        }
    }

    public void createRedSquare() {
        //create new red square
        int x1, y1;

        //Generate coordinates for red square, check it's not same as existing red square, snake, or green square
        do {
            x1 = randomGenerator.nextInt(30);
            y1 = randomGenerator.nextInt(20);
        } while (checkHasHitRedSquare(x1, y1) || checkHasHitSelf(x1, y1) || greenXCooordinate == x1 && greenYCoordinate == y1);
        redXCoordinateList.add(x1);
        redYCoordinateList.add(y1);
    }

    public void createGreenSquare() {
        int x, y;
        //Create coordinates for new Green square, if red square or snake is already at
        //these coordinates re-generate new coordinates
        do {
            x = randomGenerator.nextInt(30);
            y = randomGenerator.nextInt(20);
        } while (checkHasHitSelf(x, y) || checkHasHitRedSquare(x, y));

        greenXCooordinate = x;
        greenYCoordinate = y;
    }

    private boolean checkHasHitRedSquare(int xCoordinate, int yCoordinate) {
        boolean checkHitRedSquare = false;
        for (int i1 = 0; i1 < redXCoordinateList.size(); i1++) {
            if (redXCoordinateList.get(i1) == xCoordinate && redYCoordinateList.get(i1) == yCoordinate) {
                checkHitRedSquare = true;
            }
        }
        return checkHitRedSquare;
    }

    private boolean checkHasHitSelf(int xCoordinate, int yCoordinate) {
        boolean checkHitSelf = false;
        for (int i1 = 0; i1 < snakeXCoordinateList.size(); i1++) {
            if ((snakeXCoordinateList.get(i1) == xCoordinate) && (snakeYCoordinateList.get(i1) == yCoordinate)) {
                if (!((snakeXCoordinateList.get(snakeXCoordinateList.size() - 1) == xCoordinate) && (snakeYCoordinateList.get(snakeYCoordinateList.size() - 1) == yCoordinate))) {
                    checkHitSelf = true;
                }
            }
        }
        return checkHitSelf;
    }


    void addObserver(Observer o){
        System.out.println("add observer");
        System.out.println(observerList);
        System.out.println(observerList.size());
        observerList.add(o);
    }
    void removeObserver(Observer o){
        observerList.remove(o);
    }
    void update(){
        for (Observer observer: observerList){
            observer.update();
        }
    }

    //getters
    public int getGreenXCooordinate() {
        return greenXCooordinate;
    }


    public int getGreenYCoordinate() {
        return greenYCoordinate;
    }



    public ArrayList<Integer> getRedXCoordinateList() {
        return redXCoordinateList;
    }


    public ArrayList<Integer> getSnakeXCoordinateList() {
        return snakeXCoordinateList;
    }


    public ArrayList<Integer> getRedYCoordinateList() {
        return redYCoordinateList;
    }


    public ArrayList<Integer> getSnakeYCoordinateList() {
        return snakeYCoordinateList;
    }


    public boolean isGameOver() {
        return isGameOver;
    }


}